
class Frame:
    def __init__(self, previousFrame = None, throw1 = None, throw2 = None):
        self.__throw1 = throw1
        self.__throw2 = throw2
        self.__previousFrame = previousFrame
        self.__nextFrame = None
        if previousFrame != None :
            self.__frameNumber = previousFrame.getFrameNumber()+1
            previousFrame.setNextFrame(self)
        else:
            self.__frameNumber = 1
        self.__bonusFrame = self.__frameNumber == 11
        
    def getFrameNumber(self):
        return self.__frameNumber
    
    def setNextFrame(self,nextFrame):
        self.__nextFrame = nextFrame
    
    def getScore(self):
        if self.__throw1 is None or self.__bonusFrame:
            return None
        
        frameScore = self.getFrameScore()
        if (frameScore is None):
            return None
    
        if self.__previousFrame == None:
            return frameScore
        else:
            previousFrameScore = self.__previousFrame.getScore()
            return previousFrameScore+frameScore if previousFrameScore is not None else None

        
    
    def getFrameScore(self):
        if (self.__throw1 is None):
            return 0
        if (self.__throw2 is None):
            frameScore = self.__throw1
        else:
            frameScore = self.__throw1+self.__throw2
        if frameScore == 10 and self.__frameNumber != 11:
            bonusBalls = 2 if self.__throw1 == 10 else 1
            bonus = self.__nextFrame.getBonusScore(bonusBalls)
            if bonus is None:
                return None
            frameScore = frameScore + bonus
        return frameScore
    
                
    def getBonusScore(self,numberOfBonusBalls=1):
        if numberOfBonusBalls == 1:
            return self.__throw1
        # 2 bonus balls
        
        # 2 throws in this frame
        if self.__throw2 is not None:
            return self.__throw1+self.__throw2
        
        if self.__throw1 is None or self.__nextFrame is None :
            return None
        nextBall = self.__nextFrame.getBonusScore()
        if nextBall is None:
            return None
        return self.__throw1 + nextBall
    
    def saveThrow(self, throw):
        if self.__throw1 is None:
            self.__throw1 = throw 
            return self if (throw != 10  or self.__bonusFrame ) else self.__nextFrame
        self.__throw2 = throw
        return self.__nextFrame
    
    def printScoreCard(self):
        card = self.printLine1() +"\n" + self.printDevider() + "\n" + self.printLine2()+ "\n"
        print (card)
        
    def printLine1(self):
        score1 = " "
        score2 = " "
        if self.__throw1 is not None:
            score1=self.__throw1 if self.__throw1 != 10 else "x"
            if self.__throw2 is not None:
                score2=self.__throw2 if self.__throw1 + self.__throw2 != 10 else "/"
                if self.__bonusFrame and score2==10:
                    score2="x"
        frameString = "{}|{}".format( score1 , score2 )
        if self.__nextFrame is not None:
            frameString = frameString +  "|" + self.__nextFrame.printLine1()
        return frameString
    
    def printDevider(self):
        if self.__bonusFrame:
            return ""
        devider = "-" + str(self.__frameNumber) +"-"
        if self.__nextFrame is not None:
            devider = devider + "|" +self.__nextFrame.printDevider()
        return devider
        
    def printLine2(self):
        score = self.getScore()
        frameString = "{:3d}".format(score) if score is not None else "   "
        if self.__nextFrame is not None and not self.__bonusFrame:
            frameString = frameString + "|" + self.__nextFrame.printLine2()
        return frameString
    
            
class Game:
    def __init__(self):
        self.__frameone = Frame(None)
        frame = self.__frameone    
        for i in range (2,12):
            frame = Frame(frame)
        self.__currentFrame = self.__frameone
        
    def saveThrow(self,throw):
       
        self.__currentFrame = self.__currentFrame.saveThrow(throw)
    
    def printScoreCard(self):
        self.__frameone.printScoreCard()
            
# test code

testThrows = [10,10,2,8,3,7,1,2,5,5,2,8,7,1,0,0,10,10,10]   
game = Game ()
while len(testThrows) != 0:
    throw = testThrows.pop(0)
    game.saveThrow(throw)
    game.printScoreCard()
    
    
testThrows = [10,10,10,10,10,10,10,10,10,10,10,10]   
game = Game ()
while len(testThrows) != 0:
    throw = testThrows.pop(0)
    game.saveThrow(throw)
    game.printScoreCard()
    
    
testThrows = [2,3,2,3,2,3,2,3,2,3,2,3,2,3,2,3,2,3,2,3]
game = Game ()
while len(testThrows) != 0:
    throw = testThrows.pop(0)
    game.saveThrow(throw)
    game.printScoreCard()

